
import random
from serial import Serial
import time
import traceback
from yahoo_fin import stock_info


UBER = 45.
UPDATE_RATE = 3.

BUY_MESSAGE = 'buy more uber stock!!'
WORD_MESSAGE = ['please', 'buy', 'uber']
WORD_MESSAGE = ['     buy', '    buy', '  buy', ' buy', 'buy', 'buy uber', 'uber', '    uber', 'buy buy'] * 2

def get_stock_tuple():
    try:
        price = stock_info.get_live_price("uber")
        delta = price - UBER
        left = '%.3f' % (price / 10)
        left = left.replace('.', '`')
        #left = '{}\n\r'.format(left))
        return ('%.2f' % price, '%.2f' % delta, left, price)
    except (ValueError, KeyError):
        return ('Unknown', 'n/a', '**?* !', 0)


def write_buy_message(ser):
    for i in range(len(BUY_MESSAGE) + 8):
        ser.write(str.encode('%s\n' % BUY_MESSAGE[i:i+8]))
        time.sleep(1)


def write_word_message(ser, stock_message):
    random.shuffle(WORD_MESSAGE)
    for message in WORD_MESSAGE[0:3]:
        ser.write(str.encode('%s\n' % message))
        time.sleep(2)
        ser.write(str.encode('%s\n' % stock_message))
        time.sleep(1)


def main():
    ser = Serial('/dev/ttyUSB0', 115200)
    ser.setDTR(0)
    time.sleep(1)
    ser.setDTR(1)
    time.sleep(1)
    old_c = ''
    current_delay = UPDATE_RATE
    while True:
        (a, b, c, d) = get_stock_tuple()
        print('%s  %8s %8s      "%s"' % (time.asctime(), a, b, c))
        #ser.write(str.encode('3`845\n'))a
        if d < 32.2:
            write_word_message(ser, c)
        else:
            ser.write(str.encode('%s\n' % c))
            #ser.write(str.encode('%s\n\r' % c))
        if old_c == c:
            if current_delay / UPDATE_RATE < 3:
                current_delay = current_delay + 0.2
            else:
                current_delay = current_delay * 2
            current_delay = min(600, current_delay)
        else:
            old_c = c
            current_delay = UPDATE_RATE
        time.sleep(current_delay)


if __name__ == '__main__':
    while True:
        try:
            main()
        except KeyboardInterrupt:
            raise
        time.sleep(10)
