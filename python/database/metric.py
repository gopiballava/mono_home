# Copyright Gopiballava Flaherty 2018

from home import constants

import psycopg2
import time
import traceback


class MetricManager(object):
    def __init__(self):
        self._connect_db_cursor()

    def _connect_db_cursor(self):
        try:
            self.db_info = constants.get_db()
            self.conn = psycopg2.connect(
                "dbname='%s' user='%s' host='%s' password='%s'" %
                (self.db_info['db'], self.db_info['username'],
                 self.db_info['host'], self.db_info['password']))
            self.cur = self.conn.cursor()
        except:
            print "I am unable to connect to the database"

    def get_metric_names(self):
        """All of the metric names we know about, as a list"""
        self.cur.execute("SELECT NAME from %s" % self.db_info['name_db'])
        rows = self.cur.fetchall()
        rows = [row[0] for row in rows]
        return rows

    def insert_metric_value(self, metric_name, metric_value):
        self.cur.execute(
            "INSERT INTO metric_table (value, time_column, sensor_id) VALUES (%s, current_timestamp, (SELECT id FROM metric_names WHERE name = %s));",
            (metric_value, metric_name))
        self.conn.commit()

    def insert_ds1820_metric_value(self, ds1820_mac, metric_value):
        while True:
            try:
                self.cur.execute(
                    "INSERT INTO metric_table (value, time_column, sensor_id) VALUES (%s, current_timestamp, (SELECT id FROM metric_names WHERE ds1820_mac = %s));",
                    (metric_value, ds1820_mac))
                self.conn.commit()
                return
            except:
                traceback.print_exc()
                time.sleep(1)
                print("Reconnecting database...")
                self._connect_db_cursor()

    def create_new_metric(self, metric_name):
        self.cur.execute("INSERT INTO metric_names (name) VALUES (%s)",
                         (metric_name, ))
        self.conn.commit()

