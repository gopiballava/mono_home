
import cherrypy

class HelloWorld(object):
    @cherrypy.expose
    def add_item(self, *args, **kwargs):
        return "Hello World!"

def main():
    cherrypy.config.update({'server.socket_host': '0.0.0.0',
			'server.socket_port': 9090})
    cherrypy.quickstart(HelloWorld())


if __name__ == "__main__":
    main()
