
import sys

from neomodel import StructuredNode, StructuredRel, UniqueIdProperty, StringProperty, IntegerProperty, RelationshipTo, RelationshipFrom, ZeroOrOne, One, ZeroOrMore, config

config.DATABASE_URL = 'bolt://neo4j:esadmf1@localhost:7687'

"""
neomodel_remove_labels --db bolt://neo4j:esadmf\!@localhost:7687

neomodel_install_labels draft.py  --db bolt://neo4j:esadmf\!@localhost:7687
"""

class ConnectedToStructure(StructuredRel):
    """
    Relationships between parts of a house, or car, or other object.
    """

class PathMixin(object):
    pathname = StringProperty()
    parent_structure = RelationshipTo("PhysicalStructure", "PART_OF", model=ConnectedToStructure, cardinality=ZeroOrOne)
    child_structure = RelationshipFrom("PhysicalStructure", "PART_OF", model=ConnectedToStructure, cardinality=ZeroOrMore)
    def get_path_array(self):
        par = self.parent_structure.single()
        if par is None:
            return [self.pathname]
        else:
            return par.get_path_array() + [self.pathname]
    def get_path(self):
        pass_
class ConnectedToInterface(StructuredRel):
    """
    This represents a sensor connected to some sort of
    electrical interface, such as a JST connector.
    """
    pass


class GenericSensor(StructuredNode, PathMixin):
    name = StringProperty(unique_index=True)
    interface = RelationshipTo("ElectricalInterface", "CONNECTED_TO", model=ConnectedToInterface, cardinality=ZeroOrOne)

    def get_pin_list(self):
        return []
    def got_input_pin_state(self, pin_state):
        pass # Base class can't understand this message



class ElectricalSwitchSensor(GenericSensor):
    def get_pin_list(self):
        return [["input", "pullup", "gpio"]]
    def got_input_pin_state(self, pin_state):
        print("Switch {} state is {}".format(self.name, pin_state))


class LeakReedSwitch(ElectricalSwitchSensor):
    """
    Normally closed magnetic reed switch.
    """
    pass


class ProprietaryHumiditySensor(GenericSensor):
    def get_pin_list(self):
        return [["input", "pullup", "humidity"]]


class ConnectedToController(StructuredRel):
    pass

class ElectricalInterface(StructuredNode):
    name = StringProperty(unique_index=True)
    controller = RelationshipTo("Microcontroller", "WIRED_TO", model=ConnectedToController)
    sensor = RelationshipFrom("GenericSensor", "CONNECTED_TO", model=ConnectedToInterface, cardinality=ZeroOrOne)

    def get_pin_dict(self, which_node):
        return {}
    def got_input_pin_state(self, pin_number, pin_state):
        pass # Only GPIO interfaces understand this
        


class ConnectionGPIO(ElectricalInterface):
    gpio_pin = IntegerProperty()
    def get_pin_dict(self, which_node):
        attached_sensor = self.sensor.single()
        if attached_sensor:
            sensor_pin_list = attached_sensor.get_pin_list()
            if len(sensor_pin_list) == 1:
                return {self.gpio_pin: sensor_pin_list[0]}
            else:
                raise Exception("Expected 1 sensor pin")
        else:
            return {}
    def got_input_pin_state(self, pin_number, pin_state):
        if pin_number == self.gpio_pin:
            attached_sensor = self.sensor.single()
            if attached_sensor:
                attached_sensor.got_input_pin_state(pin_state)


class TripleJSTConnector(ConnectionGPIO):
    pass


class Microcontroller(StructuredNode):
    name = StringProperty()
    connections = RelationshipFrom(ElectricalInterface, "WIRED_TO", model=ConnectedToController)

    def get_pin_dict(self):
        retv = {}
        for interface_node in self.connections.all():
            retv.update(interface_node.get_pin_dict(self))
        return retv

    def get_input_pins(self):
        return {k: v for k, v in self.get_pin_dict().items() if 'gpio' in v}

    def got_input_pin_state(self, pin_number, pin_state):
        for connector in self.connections.all():
            connector.got_input_pin_state(pin_number, pin_state)

class esp8266(Microcontroller):
    mac_address = StringProperty()



class PhysicalStructure(StructuredNode):
    name = StringProperty()
    pathname = StringProperty()
    parent_structure = RelationshipTo("PhysicalStructure", "PART_OF", model=ConnectedToStructure, cardinality=ZeroOrOne)
    child_structure = RelationshipFrom("PhysicalStructure", "PART_OF", model=ConnectedToStructure, cardinality=ZeroOrMore)
    def get_path_array(self):
        par = self.parent_structure.single()
        if par is None:
            return [self.pathname]
        else:
            return par.get_path_array() + [self.pathname]

class PhysicalHouse(PhysicalStructure):
    pass
class PhysicalRoom(PhysicalStructure):
    pass

class ConditionDetector(StructuredNode):
    name = StringProperty()
    description = StringProperty()


if __name__ == "__main__":

    if sys.argv[-1] == "del":
        for m in Microcontroller.nodes.all():
            m.delete()
        for m in esp8266.nodes.all():
            m.delete()
        for c in TripleJSTConnector.nodes.all():
            c.delete()
        # for c in ElectricalInterface.nodes.all():
        #    c.delete()
        for s in GenericSensor.nodes.all():
            s.delete()
        for s in PhysicalStructure.nodes.all():
            s.delete()
        sys.exit(0)
    if sys.argv[-1] == "add":
        bc = esp8266(name="Basement controller", mac_address="0xdeadbeef").save()
        con = TripleJSTConnector(name="JST GPIO 8", gpio_pin=8).save()
        con.controller.connect(bc)
        leak = ProprietaryHumiditySensor(name="Humidity").save()
        leak.interface.connect(con)

        con = TripleJSTConnector(name="JST GPIO 10", gpio_pin=10).save()
        con.controller.connect(bc)
        leak = LeakReedSwitch(name="Garage leak", pathname="garage_floor_leak").save()
        leak.interface.connect(con)

        hou = PhysicalHouse(name="Alton", pathname="alton").save()
        room = PhysicalRoom(name="KrisLair", pathname="krislair").save()
        room.parent_structure.connect(hou)
        print("Room path: {}".format(room.get_path_array()))
        print("Sensor path: {}".format(leak.get_path_array()))
        leak.parent_structure.connect(room)
        print("Sensor path: {}".format(leak.get_path_array()))
        sys.exit(0)

    bc = esp8266.nodes.first_or_none(name="Basement controller")

    # if bc is None:
    #    bc = esp8266(name="Basement controller", mac_address="0xdeadbeef").save()

    #con = TripleJSTConnector.nodes.first_or_none(name="Basement Connector 8")
    # if con is None:
    #    con = TripleJSTConnector(name="Basement Connector 8").save()
    #    #bc.connections.connect(con)
    #    con.controller.connect(bc, {"gpio_pin": 8})
    #print("connect result", dir(con.controller.connect(bc)))

    # bc.delete()
    # con.delete()

    print("Input GPIO pins: {}".format(bc.get_input_pins()))
    print("All pins: {}".format(bc.get_pin_dict()))
    bc.got_input_pin_state(10, True)
    bc.got_input_pin_state(8, True)
# print(bc.connections.is_connected(con))
# print(bc.connections.all())


if False:
    all_nodes = Person.nodes.all()

    for node in all_nodes:
        print("===")
        print(node, dir(node))

    jim = Person.nodes.filter(name='Emil')
    print(list(jim))
    print(len(jim))
    if len(jim) > 1:
        for j in jim[1:]:
            j.delete()
# print(jim[0])
