import untangle

#o = untangle.parse('DBMS ER Diagram Example.vdx')
#print(dir(o))
#print(o.VisioDocument)

import xmltodict

with open('DBMS ER Diagram Example.vdx') as fd:
    doc = xmltodict.parse(fd.read())
    v = doc['VisioDocument']
#print(dir(v))

for p in v['Pages']['Page'][0]['Shapes']['Shape']:
    print("===")
    print(p['@Name'])
    print(p['@ID'])
    print(p.keys())
    print(p['@Type'])
    print(p)
    if 'Line' in p:
        print(p['Line'])
    if 'Connection' in p:
        print(p['Connection'])

for c in v['Pages']['Page'][0]['Connects']['Connect']:
    print("======")
    print(c.keys())
    print(c)
#print(v['Pages']['Page'][0]['Shapes']['Shape'][0].keys())

#v['Pages']['Page']['Shapes']['Shape'][1]
