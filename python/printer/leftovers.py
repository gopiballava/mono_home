
# Copyright Gopiballava Flaherty 2019

import datetime
from escpos import printer
from fuzzywuzzy import fuzz
import inflect
import os
import time
from unittest.mock import patch


MAC_PORT = "/dev/tty.BlueToothPrinter-HS_SPP"

LABEL_PRINT_INTERVAL = 6

SHORT_DATE_FORMAT = "%a %e %b"

LARGE_WEEKDAY_FORMAT = "%A"

STARCH_LIST = [
    "rice",
    "pieorogie",
    "potato",
    "bread",
    "muffin",
    "pasta",
    "spaghetti",
    "linguine",
    "fettucine",
    "noodle",
    "risotto",
    "oatmeal",
    "sandwich",
    "pizza",
    "hoagie",
]

p = inflect.engine()

STARCH_LIST = STARCH_LIST + [p.plural(x) for x in STARCH_LIST]


NORMAL_LIFE_DAYS = 5

SHORT_LIFE_DAYS = 2


def _get_date_tuple(short_life=False):
    """
    Returns a datetime tuple, (now, expiration_date)
    """
    made_date = datetime.datetime.now()
    if short_life:
        expiration_date = made_date + datetime.timedelta(days=SHORT_LIFE_DAYS)
    else:
        expiration_date = made_date + datetime.timedelta(days=NORMAL_LIFE_DAYS)
    return (made_date, expiration_date)


class LeftoverPrinter(object):
    def __init__(self, dummy=False):
        if dummy:
            self.esc = printer.Dummy()
            self.dummy = True
        else:
            if "Darwin" in os.uname().sysname:
                self.esc = printer.Serial(MAC_PORT)
                self.dummy = False
            else:
                self.esc = printer.Usb(0x0416, 0x5011, in_ep=0x81, out_ep=0x03)
                self.dummy = False

    def _print_calendar_sticker(self, food_name, made_date, expiration_date):
        self.esc.set(align="center", custom_size=True, width=2, height=2)
        self.esc.block_text("{}\n".format(food_name), font="b", columns=16)
        self.esc.ln()

        self.esc.set(align="center", custom_size=True, width=1, height=1)
        self.esc.textln("Made {}".format(made_date.strftime(SHORT_DATE_FORMAT)))

        self.esc.set(align="center", custom_size=True, width=1, height=1)
        self.esc.textln("Expires {}".format(expiration_date.strftime(SHORT_DATE_FORMAT)))

        self.esc.set(align="center", custom_size=True, width=3, height=3, smooth=True)
        self.esc.textln("{}".format(expiration_date.strftime(LARGE_WEEKDAY_FORMAT)))

        self.esc.print_and_feed(2)
        if self.dummy:
            print(self.esc.output.decode('utf8'))
            self.esc.clear()


    def print_pair(self, food_name):
        (made_date, expiration_date) = _get_date_tuple(short_life=is_starch(food_name))
        for i in range(2):
            self._print_calendar_sticker(food_name, made_date, expiration_date)
            self.esc.print_and_feed(2)
            time.sleep(LABEL_PRINT_INTERVAL)

def is_starch(food_name):
    matches = [fuzz.token_set_ratio(x, food_name) for x in STARCH_LIST]
    print(STARCH_LIST)
    print(food_name, matches)
    return max(matches) > 80


class DummyPrinter(object):
    def block_test(self, **kwarg):
        pass



# @patch("escpos.printer.Serial")
def main():
#     print(dir(mock_serial_printer))
#     mock_serial_printer.return_value = DummyPrinter()
    lp = LeftoverPrinter()
    lp.print_pair("Pizza")
#     print(lp.esc.is_online())
#     lp.esc.print_and_feed(2)
#     lp = LeftoverPrinter(None)
#     lp.print_pair("Burrito")
#     lp.print_pair("Seven layer burrito")
#     lp.print_pair("Mexican Rice and Beans")
#     lp.print_pair("Eat'n'Park Chile")
#     lp.print_pair("Oatmeal")
#     lp.print_pair("Corned beef hash")
#     lp.print_pair("Chicken not-parmesan sandwich")
    lp.print_pair("Steak tomato hoagie")
#    lp.print_pair("Chicken not-parmesan sandwich")


"""
from escpos.printer import Usb
u = Usb(0x0416, 0x5011, 0)

"""

if __name__ == "__main__":
    main()
