from .leftovers import is_starch, _get_date_tuple, SHORT_DATE_FORMAT

import pprint


def test_is_starch():
    assert is_starch("rice")
    assert is_starch("indian rice")
    assert is_starch("baked potato")
    assert is_starch("mashed potatoes")
    assert is_starch("english muffin")
    assert is_starch("fried rice")
    assert is_starch("spaghetti bolognese")
    assert not is_starch("palak paneer")
    assert not is_starch("hamburger")

def test_date_tuple():
    (cur, exp) = _get_date_tuple()
    (cur_b, exp_short) = _get_date_tuple(short_life=True)
    assert cur.strftime(SHORT_DATE_FORMAT) == cur_b.strftime(SHORT_DATE_FORMAT)
    assert exp.strftime(SHORT_DATE_FORMAT) != cur.strftime(SHORT_DATE_FORMAT)
    assert exp.strftime(SHORT_DATE_FORMAT) != exp_short.strftime(SHORT_DATE_FORMAT)