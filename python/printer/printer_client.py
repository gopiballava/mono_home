
import time
import urllib.request
try:
    from .leftovers import LeftoverPrinter
except:
    from leftovers import LeftoverPrinter

def get_print_once():
    resp = urllib.request.urlopen('http://199.21.105.5:9090/remove_item')
    if resp.getcode() == 200:
        for line in resp.readlines():
            if len(line) > 0:
               print("Line:'{}'".format(line.decode('utf8')))
               lp = LeftoverPrinter()
               lp.print_pair(line.decode('utf8'))
               return
    else:
        print("Error code {}".format(resp.getcode()))


def main():
    for i in range(30):
        get_print_once()
        time.sleep(2)
    print("Done.")

if __name__ == "__main__":
    main()

