#!/bin/bash

cd ../python/stock

# This is necessary so that the timeout command will terminate the Python script when you hit ctrl-C
trap 'echo Caught interrupt, exiting now;exit' INT

while true
do
    echo "Running ticker..."
    timeout --foreground -k 5 1200 python3 ticker.py
    sleep 10
done
