#!/bin/bash

cd ../python/printer

while true
do
    echo "Git update..."
    git pull
    echo "Running printer client..."
    python3 printer_client.py
    sleep 2
done
