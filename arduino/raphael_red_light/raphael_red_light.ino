#include <Arduino.h>
#ifdef ESP32
    #include <WiFi.h>
#else
    #include <ESP8266WiFi.h>
#endif
#include "fauxmoESP.h"

#include "credentials.h"

#include<weenyMo.h>

#define LED0 2


// -----------------------------------------------------------------------------

#define SERIAL_BAUDRATE     115200

#define RED_NAME "raphael red light"

// Once we're done dimming, what's the level?
int led_final_brightness = 10;
// When we begin the slow dimming process, we set this level
int led_initial_brightness = 80;
// What's the percentage drop per 500ms tick?
int dimming_rate = 5;

// Are we actively dimming?

bool is_dimming = false;
int led_current_brightness = 0;

// Sets the brightness of the onboard blue LED
void setLightBrightness(int percent)
{
  Serial.print("LED brightness is ");
  Serial.println(percent);
  analogWrite(LED0, 1023 - percent * 0.5);
  led_current_brightness = percent;
}

void start_dimming()
{
  is_dimming = true;
  setLightBrightness(led_initial_brightness);
}

void light_off()
{
  is_dimming = false;
  setLightBrightness(0);
}

//
// This function will be called when Alexa hears "switch [on | off] 'vee three'" 
// ESP8266 builtin LED is "backwards" i.e. active LOW, hence Alexa's ON=1 needs reversing
//
void onVoiceCommand(bool onoff){ 
  Serial.printf("onVoiceCommand %d\n",onoff);
  if (onoff)
  {
    start_dimming();
  }
  else
  {
    light_off();
  }
}

bool getAlexaState(){
  Serial.printf("getAlexaState %d\n", led_current_brightness > 0);
  return led_current_brightness > 0;
}

void dimming_loop()
{
    static unsigned long last_dimming = millis();
    if (millis() - last_dimming > 500) {
        if (is_dimming)
        {
          setLightBrightness(led_current_brightness - dimming_rate);
          if (led_current_brightness <= led_final_brightness)
          {
            is_dimming = false;
          }
        }
        last_dimming = millis();
    }
}
// -----------------------------------------------------------------------------
// Wifi
// -----------------------------------------------------------------------------

void wifiSetup()
{
    // Set WIFI module to STA mode
    WiFi.mode(WIFI_STA);
    // Connect
    Serial.printf("[WIFI] Connecting to %s ", WIFI_SSID);
    WiFi.begin(WIFI_SSID, WIFI_PASS);
    // Wait
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(100);
    }
    Serial.println();
    // Connected!
    Serial.printf("[WIFI] STATION Mode, SSID: %s, IP address: %s\n", WiFi.SSID().c_str(), WiFi.localIP().toString().c_str());
}

// The only constructor: const char* name, function<void(bool)> onCommand
// choose the name wisely: Alexa has very selective hearing!
//
weenyMo w("Raphael red light",onVoiceCommand);

void setup()
{
    // Init serial port and clean garbage
    Serial.begin(SERIAL_BAUDRATE);
    Serial.println();
    Serial.println();
    // Wifi
    wifiSetup();

    start_dimming();
    w.gotIPAddress(); // ready to roll...Tell Alexa to discover devices.
}


void loop() {

    // This is a sample code to output free heap every 5 seconds
    // This is a cheap way to detect memory leaks
    static unsigned long last = millis();
    if (millis() - last > 5000) {
        last = millis();
        Serial.printf("[MAIN] Free heap: %d bytes\n", ESP.getFreeHeap());
    }
    dimming_loop();
}
