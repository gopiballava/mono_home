
# Alarm entity

Scope: mutability 
Notification grouping
Acknowledgement

Operations:
Acknowledge / reset
Mute

Data:
Latching

# Edge types / API

## Sensor reading
Humidity sensor

Q: push, pull, render, data, generic vs. specific

Data vs. commands
Data API passes data objects around
No RPC, just messages? 
Node as lightweight component?

Room appropriate temperature or brightness for sleep
Fridge too warm
Data object contains "generate html stuff"
Except...mutable?
Can data object make calls to source? What is purpose?

Sensors:
Temperature
Humidity
Motion
Occupancy
Light
External light, weather, etc


Understanding:
Too hot for people

Edge: type of data, a Java object 
Edge itself is an object

Room summarizer 


Inputs?
Sensor reading, button press, update from external data / reload?
Pipeline instantiation based on message bus?

Underlying architecture: describe reality
Describe configuration 
What are goals?

First prototype: lighting
Make lights at night dim automagically 

