echo "This WAS an empty CI script"

# Fail immediately if an error:
set -e

buildkite-agent annotate "Will the tests pass? 🙀"
cd python

pip3 install -r requirements.txt

python3 -m pytest -v -s
python3 -m pytest --cov=. -v -s
#coverage run python -m pytest

codecov --token=$CODECOV_TOKEN --prefix=python

#coveralls
##COVERALLS_REPO_TOKEN=PLscz3apf5xMBv4EClF1TopD67mCJPq4o python3 -m coveralls


#echo "Checking if docker pull works..."
# This *did* work, but I don't want to run it all the time.
#docker pull tobi312/rpi-postgresql:9.6 || buildkite-agent annotate "🛥⚓️ 🤝 probably failed 😿" --style 'warning' --context 'ctx-docker'

echo "Done!"
buildkite-agent annotate "All tests passed! 🚀😻"
